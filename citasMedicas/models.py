from django.db import models

# Create your models here.

class Servicio(models.Model):
   descripcion = models.CharField(max_length=100)

class Paciente(models.Model):
   ci = models.CharField(max_length=30)
   nombre = models.CharField(max_length=100)
   apellido = models.CharField(max_length=100)

class Doctor(models.Model):
   matricula = models.CharField(max_length=20)

class Consultorio(models.Model):
   numero = models.CharField
