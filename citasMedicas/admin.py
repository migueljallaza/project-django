from django.contrib import admin

from .models import Servicio
from .models import Paciente
from .models import Doctor
from .models import Consultorio

# Register your models here.

admin.site.register(Servicio)
admin.site.register(Paciente)
admin.site.register(Doctor)
admin.site.register(Consultorio)

